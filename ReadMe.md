# Archiver 

This project was wrote in NodeJs/Typescript

## Getting started
Before all, you should **source files** with different modified/last update date and copy to the `./folder` directory.

You also have opportunity that **change the target folder path** in `./src/app.ts` with usage dir constant.

Here you can **change the day of the end of archiving** with the `dayBefore` constant. You can define a number of days count back the application from the current date. The current date is the cron tick.

If the `force` constant is `true`, the files also **will be zipped** if it already have been before. If false then no.

It have **few scripts** in `package.json` what you can run with npm.

## Try code

Run `npm install` before run the script below in this section.

Run `npm run dev` if you want to debug or use nodemon.

Run `npm run start` if you want to run only one time the app.

## Production

If you run `npm run build` command
- first it will stop the live process,
- dist, node_modules directories and nohup files will be deleted,
- dependencies will be installed again,
- the application will be build to disc folder,
- finally the application will be run by nohup in background.

You can stop the background process manually with `npm run stop` command.
