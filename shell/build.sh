#!/bin/bash

echo "start ArchiverCron build process"

npm run stop
echo "running is stopped"

rm -rf ./disc ./node_modules ./nohup.* &&
echo "old files are removed"

(npm install && tsc) &&
echo "dependencies is installed and javascript is built"

nohup node ./disc/index.js > nohup.out 2>&1 & echo $! > nohup.pid
echo "app is run with nohup"
