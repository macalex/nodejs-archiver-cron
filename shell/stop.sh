#!/bin/bash

FILE=nohup.pid
if test -f "$FILE"; then
    kill -9 `cat $FILE` && rm -rf $FILE
    echo "killed"
fi
echo "process end"
