import * as path from 'path';

import { archiveFilesOlderThen } from './archiver';

const dir: string = path.join(__dirname, '..', 'folder');
const dayBefore: number = 30;
const force: boolean = false;

const run = (): void => {
  archiveFilesOlderThen(dayBefore, dir, force);
};

export default run;
