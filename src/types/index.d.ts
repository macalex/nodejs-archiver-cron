export declare type CallbackType = (err: any, path: string, files: string, force: boolean) => void;
