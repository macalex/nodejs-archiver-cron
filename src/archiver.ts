import { doOnFilesBeforeDate } from './files';
import { zip } from './zipper';

const dayMs: number = 1000 * 60 * 60 * 24;

export const archiveFilesOlderThen = (dayDiff: number, dirPath: string, force: boolean): void => {
  const nowMs: number = (new Date()).getTime();
  const endDateMs: number = nowMs - (dayMs * dayDiff);

  doOnFilesBeforeDate(dirPath, endDateMs, zip, force);
};
