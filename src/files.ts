import * as fs from 'fs';
import * as path from 'path';

import { CallbackType } from './types';

export const doOnFilesBeforeDate = (dir: string, date: number, callback: CallbackType, force: boolean): void => {
  fs.readdir(dir, (err: any, files: string[]) => {
    if (err) { return {error: true, err}; }
    if (files.length) {
      const filteredFiles: string[] = files.filter((file: string) => {
        const { mtimeMs } = fs.statSync(path.join(dir, '/', file));
        if (mtimeMs < date) {
          callback(err, path.join(dir, '/'), file, force);
        }
      });
      return {success: true, files: filteredFiles}
    }
  });
};
