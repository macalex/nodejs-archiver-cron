import * as cron from 'node-cron';
import app from './app';

// TODO: This row below would be useful for test:
// cron.schedule('* * * * *', () => {
cron.schedule('0 0 1 * *', () => {
  try {
    app();
  } catch (err) {
    console.log({error: true, err});
  }
}, {
  timezone: 'Europe/Budapest'
});
