import * as fs from 'fs';
import * as Path from 'path';
import * as zlib from 'zlib';

import { CallbackType } from './types';

export const zip: CallbackType = (err, path, filename, force): Promise<any | true> => {
  return new Promise((resolve, reject) => {
    const zippedFile: string = `${path}${Path.parse(filename).name}.gz`;
    if (!fs.existsSync(zippedFile) || force) {
      const fileContents: fs.ReadStream = fs.createReadStream(`${path}${filename}`);
      const writeStream: fs.WriteStream = fs.createWriteStream(zippedFile);
      const zip: zlib.Gzip = zlib.createGzip();
      fileContents.pipe(zip).pipe(writeStream).on('finish', (err: any) => {
        if (err) return reject(err);
        else {
          // TODO: Have to remove if it isn't necessary in production
          console.log(`zip[${new Date()}] - ${zippedFile}`);
          resolve();
        }
      })
    }
  })
};
